//version constants for the Kotlin DSL dependencies
object Versions {

    const val picassoLibrary = "2.71828"
    const val androidAnnotation = "1.1.0"
    const val daggerOnly = "2.28"
    const val vectorDrawable = "1.1.0"
    const val adapterRxJava2 = "2.9.0"
    const val adapterRxJava3 = adapterRxJava2
    const val rxJava3 = "3.0.3"
    const val rxKotlin3 = "3.0.0"
    const val rxRelay = "3.0.0"
    const val rxKotlin = "2.4.0"
    const val rxAndroid = "2.1.1"
    const val rxJava = "2.2.19"
    const val activityKtx = "1.2.0-beta01"
    const val fragmentKts = "1.3.0-beta01"
    const val recyclerView = "1.2.0-alpha06"
    const val navigationVersion = "2.3.1"

    /** Android UI */
    const val appCompatVersion = "1.2.0"
    const val materialLibrary = "1.2.1"
    const val constraintLayout = "2.0.4"

    /**
     * Helper Libraries
     */
    const val sdpAndroid = "1.0.6"
    const val sspAndroid = sdpAndroid
    const val blankj = "1.29.0"

    /**
     * Networking Libraries
     */
    const val okHttp = "4.9.0"
    const val retrofit = "2.9.0"
    const val serialization = "1.0.0-RC"
    const val converterGson = "2.9.0"
    const val loggingInterceptor = "4.9.0"
    const val gsonLibrary = "2.8.6"

    const val daggerHilt = "2.28-alpha"
    const val daggerHiltCompiler = daggerHilt
    const val hiltLifeCycle = "1.0.0-alpha02"
    const val hiltCompiler = hiltLifeCycle

    // lifecycle
    const val lifecycleViewModel = "2.2.0"
    const val lifecycleLiveData = lifecycleViewModel
    const val lifecycleRuntime = lifecycleViewModel
    const val lifecycleViewModelSavedState = lifecycleViewModel
    const val commonJava = lifecycleViewModel
    const val lifeCyclerCompiler = "2.2.0"


    const val coreKtx = "1.2.0"
    const val gradleVersion = "4.1.1"
    const val kotlinVersion = "1.4.0"
    const val safeArgsPlugin = "2.3.1"
    const val hiltPlugin = daggerHilt
}
