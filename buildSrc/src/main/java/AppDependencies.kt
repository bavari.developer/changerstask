import org.gradle.api.artifacts.dsl.DependencyHandler

/**
 * As static dependencies needed to be exist for making Gradle easier.
 * define all the required dependencies here and change any version
 * when there is any upgrades when needed
 */
object AppDependencies {

    /** android ui */
    private const val appCompatLibrary = "androidx.appcompat:appcompat:${Versions.appCompatVersion}"
    private const val appAnnotation = "androidx.annotation:annotation:${Versions.androidAnnotation}"
    private const val materialDesignLibrary =
        "com.google.android.material:material:${Versions.materialLibrary}"
    private const val constraintLayoutLibrary =
        "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    private const val recyclerView = "androidx.recyclerview:recyclerview:${Versions.recyclerView}"

    /** android helpers */
    private const val sdpLibrary = "com.intuit.sdp:sdp-android:${Versions.sdpAndroid}"
    private const val sspLibrary = "com.intuit.ssp:ssp-android:${Versions.sspAndroid}"
    private const val blankJ = "com.blankj:utilcodex:${Versions.blankj}"

    /** Vectors */
    private const val vectorDrawable =
        "androidx.vectordrawable:vectordrawable:${Versions.vectorDrawable}"

    /** Networking */
    private const val okHttp = "com.squareup.okhttp3:okhttp:${Versions.okHttp}"
    private const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    private const val converterGson =
        "com.squareup.retrofit2:converter-gson:${Versions.converterGson}"
    private const val loggingInterceptor =
        "com.squareup.okhttp3:logging-interceptor:${Versions.loggingInterceptor}"
    private const val serialization =
        "org.jetbrains.kotlinx:kotlinx-serialization-core:${Versions.serialization}"
    private const val adapterRxJava2 =
        "com.squareup.retrofit2:adapter-rxjava2:${Versions.adapterRxJava2}"
    private const val adapterRxJava3 =
        "com.squareup.retrofit2:adapter-rxjava3:${Versions.adapterRxJava3}"

    /** has been replaced due to the error of : Your current kotlinx.serialization core version is too low, while current Kotlin compiler plugin 1.4.20 requires at least 1.0-M1-SNAPSHOT. Please update your kotlinx.serialization runtime dependency.*/
    private const val gsonGoogle = "com.google.code.gson:gson:${Versions.gsonLibrary}"

    /** coroutines , Hilt , Dagger, Rx */
    private const val daggerOnly = "com.google.dagger:dagger:${Versions.daggerOnly}"
    private const val daggerAndroidApi = "com.google.dagger:dagger-android:${Versions.daggerOnly}"
    private const val daggerHiltAndroid = "com.google.dagger:hilt-android:${Versions.daggerHilt}"
    private const val daggerHiltAndroidCompiler =
        "com.google.dagger:hilt-android-compiler:${Versions.daggerHiltCompiler}"
    private const val androidxHiltCompilerKapt =
        "androidx.hilt:hilt-compiler:${Versions.hiltCompiler}"

    //    private const val daggerCore = "com.google.dagger:dagger-compiler:${Versions.daggerOnly}"
    private const val androidxHiltLifecycleViewModel =
        "androidx.hilt:hilt-lifecycle-viewmodel:${Versions.hiltLifeCycle}"
    private const val rxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroid}"
    private const val rxKotlin = "io.reactivex.rxjava2:rxkotlin:${Versions.rxKotlin}"
    private const val rxRelay = "com.jakewharton.rxrelay3:rxrelay:${Versions.rxRelay}"
    private const val rxJava3Kotlin = "io.reactivex.rxjava3:rxkotlin:${Versions.rxKotlin3}"
    private const val rxJava3 = "io.reactivex.rxjava3:rxjava:${Versions.rxJava3}"
    private const val picasso = "com.squareup.picasso:picasso:${Versions.picassoLibrary}"

    /** Life Cycles Libraries
     *  Ref: https://developer.android.com/jetpack/androidx/releases/lifecycle*/
    private const val lifecycleViewModel =
        "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycleViewModel}"
    private const val lifecycleLiveData =
        "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycleLiveData}"
    private const val lifecycleRuntime =
        "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifecycleRuntime}"
    private const val lifecycleViewModelSavedState =
        "androidx.lifecycle:lifecycle-viewmodel-savedstate:${Versions.lifecycleViewModelSavedState}"
    private const val lifecycleJava8 =
        "androidx.lifecycle:lifecycle-common-java8:${Versions.commonJava}"
    private const val lifecycleService =
        "androidx.lifecycle:lifecycle-service:${Versions.commonJava}"
    private const val lifecycleProcess =
        "androidx.lifecycle:lifecycle-process:${Versions.commonJava}"
    private const val lifecycleReactStream =
        "androidx.lifecycle:lifecycle-reactivestreams-ktx:${Versions.commonJava}"

    /* should use kapt*/
    private const val lifeCycleCompilerKapt =
        "androidx.lifecycle:lifecycle-compiler:${Versions.lifeCyclerCompiler}"

    /** Core Libraries */
    private const val coreKtx = "androidx.core:core-ktx:${Versions.coreKtx}"
    private const val rxJava = "io.reactivex.rxjava2:rxjava:${Versions.rxJava}"

    /** Navigation */
    private const val navigationFragment =
        "androidx.navigation:navigation-fragment-ktx:${Versions.navigationVersion}"
    private const val navigationUi =
        "androidx.navigation:navigation-ui-ktx:${Versions.navigationVersion}"

    private const val fragments = "androidx.fragment:fragment-ktx:${Versions.fragmentKts}"
    private const val activityKtx = "androidx.activity:activity-ktx:${Versions.activityKtx}"

    val appUiLibraries = arrayListOf<String>().apply {
        add(appCompatLibrary)
        add(constraintLayoutLibrary)
        add(materialDesignLibrary)
        add(recyclerView)
    }

    val appHelpers = arrayListOf<String>().apply {
        add(sdpLibrary)
        add(sspLibrary)
        add(blankJ)
        add(picasso)
    }


    val appJetpack = arrayListOf<String>().apply {
        add(fragments)
        add(activityKtx)
    }

    val appNetWorking = arrayListOf<String>().apply {
        add(retrofit)
        add(okHttp)
        add(converterGson)
        add(loggingInterceptor)
        add(serialization)
        add(gsonGoogle)
        add(adapterRxJava2)
        add(adapterRxJava3)
    }

    val appCoreLibraries = arrayListOf<String>().apply {
        add(coreKtx)
        add(navigationFragment)
        add(navigationUi)
    }

    val appDI = arrayListOf<String>().apply {
        add(daggerHiltAndroid)
        add(daggerOnly)
        add(androidxHiltLifecycleViewModel)
        add(rxAndroid)
        add(rxKotlin)
        add(rxRelay)
        add(rxJava3Kotlin)
        add(rxJava3)
        add(rxJava)
        add(daggerAndroidApi)
    }

    val appKaptDependencies = arrayListOf<String>().apply {
        add(daggerHiltAndroidCompiler)
        add(androidxHiltCompilerKapt)
        add(appAnnotation)
        add(lifeCycleCompilerKapt)
    }

    val appVectors = arrayListOf<String>().apply {
        add(vectorDrawable)
    }

    val appLifeCycle = arrayListOf<String>().apply {
        add(lifecycleLiveData)
        add(lifecycleRuntime)
        add(lifecycleViewModel)
        add(lifecycleViewModelSavedState)
        add(lifecycleJava8)
        add(lifecycleService)
        add(lifecycleProcess)
        add(lifecycleReactStream)
    }
}


//util functions for adding the different type dependencies from build.gradle file
fun DependencyHandler.kapt(list: List<String>) {
    list.forEach { dependency ->
        add("kapt", dependency)
    }
}

fun DependencyHandler.implementation(list: List<String>) {
    list.forEach { dependency ->
        add("implementation", dependency)
    }
}

fun DependencyHandler.androidTestImplementation(list: List<String>) {
    list.forEach { dependency ->
        add("androidTestImplementation", dependency)
    }
}

fun DependencyHandler.testImplementation(list: List<String>) {
    list.forEach { dependency ->
        add("testImplementation", dependency)
    }
}
