//app level config constants
object AppConfig {
    const val compileSdk = 30
    const val minSdk = 21
    const val targetSdk = 30
    const val versionCode = 1
    const val versionName = "1.0.0"
    const val buildToolsVersion = "29.0.3"
    const val applicationId = "com.khalodark.changerstask"
    const val androidTestInstrumentation = "androidx.test.runner.AndroidJUnitRunner"
    const val DEV_BASE_URL_DEBUG = "https://api.thedogapi.com/v1/"
}