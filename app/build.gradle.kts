import AppDependencies.appCoreLibraries
import AppDependencies.appDI
import AppDependencies.appHelpers
import AppDependencies.appJetpack
import AppDependencies.appKaptDependencies
import AppDependencies.appLifeCycle
import AppDependencies.appNetWorking
import AppDependencies.appUiLibraries
import AppDependencies.appVectors

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
    id("androidx.navigation.safeargs.kotlin")
    id("kotlin-android-extensions")
    id("kotlin-android")
    id("dagger.hilt.android.plugin")
}
android {

    lintOptions {
        isCheckReleaseBuilds = false
        isAbortOnError = false
    }
    compileSdkVersion(AppConfig.compileSdk)
    buildToolsVersion(AppConfig.buildToolsVersion)

    defaultConfig {
        applicationId(AppConfig.applicationId)
        minSdkVersion(AppConfig.minSdk)
        targetSdkVersion(AppConfig.targetSdk)
        versionCode = AppConfig.versionCode
        versionName = AppConfig.versionName
        vectorDrawables.useSupportLibrary = true
        testInstrumentationRunner(AppConfig.androidTestInstrumentation)
        buildConfigField("String", "BASE_URL", "\"" + AppConfig.DEV_BASE_URL_DEBUG + "\"")
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        getByName("debug") {
            signingConfig = signingConfigs.getByName("debug")
            buildConfigField("String", "BASE_URL", "\"" + AppConfig.DEV_BASE_URL_DEBUG + "\"")
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            isDebuggable = true
            isMinifyEnabled = false
            isShrinkResources = false
        }

        compileOptions {
            sourceCompatibility = JavaVersion.VERSION_1_8
            targetCompatibility = JavaVersion.VERSION_1_8
        }

        kotlinOptions {
            jvmTarget = "1.8"
        }

        lintOptions {
            isAbortOnError = true
            disable("UnusedResources") // https://issuetracker.google.com/issues/63150366
            disable("InvalidPackage")
            disable("VectorPath")
            disable("TrustAllX509TrustManager")
        }
        kapt {
            correctErrorTypes = true
        }

        dexOptions {
            incremental = true
            javaMaxHeapSize = "6g"
        }

        packagingOptions {
            exclude("META-INF/DEPENDENCIES")
            exclude("META-INF/LICENSE")
            exclude("META-INF/LICENSE.txt")
            exclude("META-INF/license.txt")
            exclude("META-INF/NOTICE")
            exclude("META-INF/NOTICE.txt")
            exclude("META-INF/notice.txt")
            exclude("META-INF/ASL2.0")
            exclude("META-INF/*.kotlin_module")
        }
    }

    dependencies {
        //std lib
        implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
        implementation(appUiLibraries)
        implementation(appNetWorking)
        implementation(appLifeCycle)
        implementation(appHelpers)
        implementation(appDI)
        implementation(appVectors)
        implementation(appCoreLibraries)
        implementation(appJetpack)
        kapt(appKaptDependencies)
    }
}