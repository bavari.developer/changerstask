package com.khalodark.changerstask.app

import android.app.Application
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.blankj.utilcode.util.Utils
import dagger.hilt.android.HiltAndroidApp

/**
 * Basic Application Class, implementing the lifecycleOwner interface for
 * handling LifeCycle Changes without need to change / add any code inside
 * Fragment / activity
 */
@HiltAndroidApp
class ChangersApplication : Application(), LifecycleOwner {

    override fun onCreate() {
        super.onCreate()
        Utils.init(this)
    }

    override fun getLifecycle(): Lifecycle {
        return lifecycle
    }
}