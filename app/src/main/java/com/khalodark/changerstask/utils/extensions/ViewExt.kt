package com.khalodark.changerstask.utils.extensions

import android.app.Activity
import android.content.Context
import android.graphics.Paint
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat.getColor
import com.google.android.material.snackbar.Snackbar
import com.khalodark.changerstask.R
import com.khalodark.changerstask.base.BaseActivity
import com.squareup.picasso.Picasso


fun <T : View> T.click(block: (T) -> Unit) = setOnClickListener { block(it as T) }
fun <T : View> T.longClick(block: (T) -> Boolean) = setOnLongClickListener { block(it as T) }

fun BaseActivity.hideKeyboard() {
    val target = this
    val imm: InputMethodManager =
        target.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    //Find the currently focused view, so we can grab the correct window token from it.
    var view: View? = target.currentFocus
    //If no view currently has focus, create a new one, just so we can grab a window token from it
    if (view == null) {
        view = View(target)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

inline fun <reified T : View> View.find(@IdRes id: Int): T = findViewById<T>(id)

inline fun <reified T : View> View.findOptional(@IdRes id: Int): T? = findViewById(id) as? T

fun Context.getColorCompat(color: Int) = getColor(this, color)

fun Context.inflateLayout(
    @LayoutRes layoutId: Int,
    parent: ViewGroup? = null,
    attachToRoot: Boolean = false
): View = LayoutInflater.from(this).inflate(layoutId, parent, attachToRoot)


fun TextView.underLine() {
    paint.flags = paint.flags or Paint.UNDERLINE_TEXT_FLAG
    paint.isAntiAlias = true
}

fun Activity.showMessage(message: String?) {
    if (message != null) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}

fun Activity.showCustomSnackBar(message: String) {
    val snackBar = Snackbar.make(
        findViewById(android.R.id.content),
        message, Snackbar.LENGTH_SHORT
    )
    val layout = snackBar.view as Snackbar.SnackbarLayout
    layout.findViewById<TextView>(com.google.android.material.R.id.snackbar_text).visibility =
        View.INVISIBLE
    val sbView = snackBar.view
    val params: FrameLayout.LayoutParams = sbView.layoutParams as FrameLayout.LayoutParams
    sbView.setBackgroundColor(getColor(this, android.R.color.holo_red_dark))
    params.gravity = Gravity.TOP
    val snackView: View = this.layoutInflater.inflate(R.layout.custom_snack_bar_layout, null)
    (snackView.findViewById<View>(R.id.text) as AppCompatTextView).text = message
    layout.addView(snackView, 0)
    snackBar.show()
}

/**
 * Show ImageView using Picasso Library
 */
fun AppCompatImageView.picassoImage(url: String?) {
    val target = this
    Picasso
        .get()
        .load(url)
        .centerCrop()
        .fit()
        .noFade()
        .into(target)
}
