package com.khalodark.changerstask.utils.helpers

enum class ApiStatus {
    SUCCESS,
    ERROR,
    LOADING,
    FAIL
}