package com.khalodark.changerstask.ui

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.khalodark.changerstask.R
import com.khalodark.changerstask.base.BaseActivity
import com.khalodark.changerstask.data.DogsViewModel
import com.khalodark.changerstask.data.model.DogDataResponse
import com.khalodark.changerstask.utils.extensions.showMessage
import com.khalodark.changerstask.utils.helpers.ApiStatus
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class MainActivity : BaseActivity(R.layout.activity_main) {


    /**
     * private instance of injectable viewModel DogsViewModel
     */
    private val dogViewModel by viewModels<DogsViewModel>()
    lateinit var mDogsImagesAdapter: DogsImagesAdapter
    private lateinit var dogsBreedList: List<DogDataResponse>
    private lateinit var spinnerAdapter: ArrayAdapter<DogDataResponse>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkInternetConnectionThen {
            dogViewModel.getDogsBreed()
        }
        listener()
    }

    /**
     * define listeners:
     * Note: according to api, I have choose the dog breeds names
     * and distinct them to be able to select any and fetch pictures
     * according to selected breed
     */
    private fun listener() {
        dogViewModel.dogsResponseObserver.observe(this, {
            when (it.status) {
                ApiStatus.SUCCESS -> {
                    hideLoading()
                    dogsBreedList = it.data!!
                    val dogsBreedDistnicted = dogsBreedList.distinctBy { it ->
                        it.breedGroup
                    }
                    spinnerAdapter =
                        ArrayAdapter(this, android.R.layout.simple_list_item_1, dogsBreedDistnicted)
                    spinnerDogBreedGroup.adapter = spinnerAdapter
                }
                ApiStatus.FAIL -> {
                    hideLoading()
                    showMessage(it.message)
                }
                ApiStatus.LOADING -> showLoading()
                ApiStatus.ERROR -> {
                    hideLoading()
                    showMessage(it.message)
                }
            }
        })

        /**
         * Select listener for listening to the chosen item
         */
        spinnerDogBreedGroup.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // do not do anything
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val (dogsBreedListForAdapter, anotherValue) = dogsBreedList.partition {
                    it.breedGroup == parent!!.getItemAtPosition(position).toString()
                }

                mDogsImagesAdapter = DogsImagesAdapter(dogsBreedListForAdapter)
                recyclerView.layoutManager = GridLayoutManager(this@MainActivity, 3)
                recyclerView.adapter = mDogsImagesAdapter
                mDogsImagesAdapter.notifyDataSetChanged()
            }
        }
    }
}