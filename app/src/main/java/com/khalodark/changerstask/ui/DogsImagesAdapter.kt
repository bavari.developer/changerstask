package com.khalodark.changerstask.ui

import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.khalodark.changerstask.R
import com.khalodark.changerstask.data.model.DogDataResponse
import com.khalodark.changerstask.utils.extensions.inflateLayout
import com.khalodark.changerstask.utils.extensions.picassoImage

/**
 * The adapter that is responsible for fetching images according
 * to the list passed
 */
open class DogsImagesAdapter(
    private val DogResponse: List<DogDataResponse>
) : RecyclerView.Adapter<DogsImagesAdapter.ViewStateAdapter>() {

    class ViewStateAdapter(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imgDog: AppCompatImageView = itemView.findViewById(R.id.aciv)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewStateAdapter(parent.context.inflateLayout(R.layout.adapter_imageview))

    override fun onBindViewHolder(holder: ViewStateAdapter, position: Int) {
        holder.imgDog.picassoImage(DogResponse[position].image!!.url)
        holder.setIsRecyclable(false)
    }

    override fun getItemCount() = DogResponse.size
}