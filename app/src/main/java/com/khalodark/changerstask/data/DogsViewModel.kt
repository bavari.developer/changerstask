package com.khalodark.changerstask.data

import androidx.hilt.lifecycle.ViewModelInject
import com.khalodark.changerstask.base.BaseResponse
import com.khalodark.changerstask.base.BaseViewModel
import com.khalodark.changerstask.data.model.DogDataResponse
import com.khalodark.changerstask.data.remote.ApiService
import com.khalodark.changerstask.utils.helpers.SingleLiveEvent
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import retrofit2.Response

/**
 * Injected ViewModel to use on the called activity / fragment
 */
class DogsViewModel @ViewModelInject constructor(
    private val apiService: ApiService
) : BaseViewModel() {

    /**
     * Holding the response through observer, should be implemented in the called context
     * to fetch / catch the results and show them.
     */
    val dogsResponseObserver = SingleLiveEvent<BaseResponse<List<DogDataResponse>>>()

    /**
     * Perform calling to API by subscribing the event of Dog Breeds
     */
    fun getDogsBreed() {
        subscribeSingle(
            apiService.getDogBreed(102),
            object : SingleObserver<Response<List<DogDataResponse>>> {
                override fun onSubscribe(d: Disposable) {
                    addDisposable(d)
                    dogsResponseObserver.value = BaseResponse.loading(null)
                }

                override fun onSuccess(t: Response<List<DogDataResponse>>) {
                    if (t.isSuccessful) {
                        dogsResponseObserver.value = BaseResponse.success(t.body()!!)
                    } else {
                        dogsResponseObserver.value = BaseResponse.errorResponse(null, t.errorBody())
                    }
                }

                override fun onError(e: Throwable) {
                    dogsResponseObserver.value = BaseResponse.error(e.message!!, null)
                }
            }, dogsResponseObserver
        )
    }


}