package com.khalodark.changerstask.data.model

import com.google.gson.annotations.SerializedName

/**
 * Data class object for fetching results
 */
data class DogDataResponse(

    @field:SerializedName("image")
    val image: Image? = null,

    @field:SerializedName("life_span")
    val lifeSpan: String? = null,

    @field:SerializedName("breed_group")
    var breedGroup: String? = "Not Defined",

    @field:SerializedName("temperament")
    val temperament: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("weight")
    val weight: Weight? = null,

    @field:SerializedName("bred_for")
    val bredFor: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("reference_image_id")
    val referenceImageId: String? = null,

    @field:SerializedName("height")
    val height: Height? = null

) {
    override fun toString(): String {
        if (breedGroup!!.isEmpty())
            return "Not Defined"
        return breedGroup!!
    }
}

data class Height(

    @field:SerializedName("metric")
    val metric: String? = null,

    @field:SerializedName("imperial")
    val imperial: String? = null
)

data class Image(

    @field:SerializedName("width")
    val width: Int? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("url")
    val url: String? = null,

    @field:SerializedName("height")
    val height: Int? = null
)

data class Weight(

    @field:SerializedName("metric")
    val metric: String? = null,

    @field:SerializedName("imperial")
    val imperial: String? = null
)
