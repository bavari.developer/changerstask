package com.khalodark.changerstask.data.remote

import com.khalodark.changerstask.base.AppConstants
import com.khalodark.changerstask.data.model.DogDataResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface ApiService {

    /**
     * Get Categories
     */
    @GET("breeds?page=0")
    fun getDogBreed(
        @Query("limit") page: Int,
        @Header("x-api-key") token: String = AppConstants.API_KEY
    ): Single<Response<List<DogDataResponse>>>

}