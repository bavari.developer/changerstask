package com.khalodark.changerstask.base

import com.google.gson.Gson
import com.khalodark.changerstask.utils.helpers.ApiStatus
import okhttp3.ResponseBody

/**
 * Organizing the status of API calling by this Class, where each method will do something important
 * success = when the request successfully respond, fetch the response according to the passed object.
 * error = when there are any error in the request, parsing the error
 * loading = on loading the results - requesting API
 */
data class BaseResponse<out T>(val status: ApiStatus, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T): BaseResponse<T> {
            return BaseResponse(ApiStatus.SUCCESS, data, null)
        }

        fun <T> success(): BaseResponse<T> {
            return BaseResponse(ApiStatus.SUCCESS, null, null)
        }

        fun <T> error(msg: String, data: T?): BaseResponse<T> {
            return BaseResponse(ApiStatus.ERROR, data, msg)
        }

        fun <T> loading(data: T?): BaseResponse<T> {
            return BaseResponse(ApiStatus.LOADING, data, null)
        }

        fun <T> errorResponse(data: T?, errorBody: ResponseBody?): BaseResponse<T> {
            val error = errorBody!!.string()
            return BaseResponse(ApiStatus.FAIL, data, error)
        }

        private fun <TB> parseError(error: String, t: Class<TB>): TB {
            return Gson().fromJson(error, t)
        }
    }

}