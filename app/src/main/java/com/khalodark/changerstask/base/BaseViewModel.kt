package com.khalodark.changerstask.base


import androidx.lifecycle.ViewModel
import com.blankj.utilcode.util.GsonUtils
import com.khalodark.changerstask.utils.helpers.SingleLiveEvent
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response


abstract class BaseViewModel : ViewModel() {

    private val disposable: CompositeDisposable? = CompositeDisposable()

    /**
     * Clearing Disposals that still holds values
     */
    private fun clear() {
        if (disposable != null) {
            if (disposable.size() > 0) {
                if (!disposable.isDisposed) {
                    disposable.dispose()
                    disposable.clear()
                }
            }
        }
    }

    /**
     * Remove Disposals
     */
    fun removeDisposable(d: Disposable?) {
        if (disposable != null) {
            if (disposable.size() > 0) {
                if (!disposable.isDisposed) {
                    disposable.dispose()
                    disposable.remove(d!!)
                } else {
                    disposable.remove(d!!)
                }
            }
        }
    }

    /**
     * Parsing errors when exists in the API
     */
    fun <T> parseError(error: String, t: Class<T>): T {
        return GsonUtils.fromJson(error, t)
    }

    /**
     * to make calls executable on Main Thread
     */
    open fun <T> mainThread(single: Single<T>): Single<T> {
        return single.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    /**
     * Subscribing Single Observer
     */
    open fun <T> subscribeSingle(single: Single<T>, observable: SingleObserver<T>) {
        mainThread(single).subscribe(observable)
    }

    /**
     * For controlling response and status of API changes
     * used this method for sending received values to the
     * appropriated methods.
     */
    open fun <T> subscribeSingle(
        single: Single<Response<T>>,
        observable: SingleObserver<Response<T>>,
        event: SingleLiveEvent<BaseResponse<T>>
    ) {
        mainThread(single).subscribe(object : SingleObserver<Response<T>> {
            override fun onSubscribe(d: Disposable) {
                addDisposable(d)
                event.value = BaseResponse.loading(null)
                observable.onSubscribe(d)
            }

            override fun onSuccess(t: Response<T>) {
                observable.onSuccess(t)
            }

            override fun onError(e: Throwable) {
                event.value = BaseResponse.error(e.message!!, null)
                observable.onError(e)
            }

        })
    }

    fun addDisposable(d: Disposable?) {
        disposable!!.add(d!!)
    }

    override fun onCleared() {
        super.onCleared()
        clear()
    }
}