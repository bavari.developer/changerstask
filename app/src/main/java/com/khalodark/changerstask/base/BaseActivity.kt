package com.khalodark.changerstask.base

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import com.blankj.utilcode.util.NetworkUtils
import com.khalodark.changerstask.R
import com.khalodark.changerstask.utils.extensions.showCustomSnackBar
import kotlinx.android.synthetic.main.activity_main.*

abstract class BaseActivity(@LayoutRes layout: Int) : AppCompatActivity(layout) {

    /**
     * Provide loading start method
     */
    open fun showLoading() {
        progressBar.visibility = View.VISIBLE
        progressBar.isIndeterminate = true
    }

    /**
     * Provide loading stop method
     */
    open fun hideLoading() {
        progressBar.visibility = View.GONE
        progressBar.isIndeterminate = false
    }

    /**
     * Open Activity Intent Function
     * @param currentContext - the current activity stand for
     * @param tClass - where to go class
     * @param isStacked - should be finished or stayed
     * @param bundle - holding any bundle if exists
     */
    open fun <T : Activity> openActivityStack(
        currentContext: Activity,
        tClass: Class<T>?,
        isStacked: Boolean,
        bundle: Bundle?
    ) {
        val intent = Intent(currentContext, tClass)
        if (!isStacked) {
            intent.flags = intent.flags or Intent.FLAG_ACTIVITY_NO_HISTORY
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        if (bundle != null) {
            intent.putExtra("data", bundle)
        }
        currentContext.startActivity(intent)
    }

    /**
     * Function that checks internet connection
     */
    fun checkInternetConnectionThen(function: () -> Unit) {
        if (!NetworkUtils.isConnected()) {
            showErrorMessage(getString(R.string.no_internet_connection))
        } else {
            function()
        }
    }

    fun showErrorMessage(message: String) {
        showCustomSnackBar(message)
    }
}