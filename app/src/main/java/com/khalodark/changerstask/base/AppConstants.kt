package com.khalodark.changerstask.base

import java.util.concurrent.TimeUnit

/**
 * Constants to use in app
 */
object AppConstants {
    const val API_KEY = "7387b76f-1fd7-4206-988f-9044149678bd"
    const val TIME_OUT: Long = 1
    val TIME_UNIT = TimeUnit.MINUTES

}