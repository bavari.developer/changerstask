package com.khalodark.changerstask.di

import com.khalodark.changerstask.BuildConfig
import com.khalodark.changerstask.base.AppConstants
import com.khalodark.changerstask.data.remote.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * providing using @Provide annotation for classes that cannot
 * be injected in their constructor
 */
@Module
@InstallIn(ApplicationComponent::class)
class ApplicationModule {
    /**
     * Providing Base Url
     */
    @Provides
    fun provideBaseUrl() = BuildConfig.BASE_URL

    /**
     * Ok Http Client library to control and provide it through hilt
     * main usage to read the logger of the http requests , in addition
     * to time readout, connection timeout and write timeout
     */
    @Provides
    @Singleton
    fun provideOkHttpClient() = run {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        OkHttpClient.Builder()
            .addNetworkInterceptor(loggingInterceptor)
            .readTimeout(AppConstants.TIME_OUT, AppConstants.TIME_UNIT)
            .connectTimeout(AppConstants.TIME_OUT, AppConstants.TIME_UNIT)
            .writeTimeout(AppConstants.TIME_OUT, AppConstants.TIME_UNIT)
            .build()
    }

    /**
     * Main Retrofit Provider
     */
    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        baseUrl: String
    ): Retrofit =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()

    /**
     * Provide a singleton of class ApiService
     */
    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(
        ApiService::class.java
    )
}