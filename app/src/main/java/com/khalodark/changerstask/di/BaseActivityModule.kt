package com.khalodark.changerstask.di

import android.app.Activity
import com.khalodark.changerstask.base.BaseActivity
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

/**
 * Make sure every activity is extended from base class
 */
@Module
@InstallIn(ActivityComponent::class)
object BaseActivityModule {

    @Provides
    fun provideBaseActivity(activity: Activity): BaseActivity {
        check(activity is BaseActivity) { "Every Activity is expected to extend BaseActivity" }
        return activity
    }
}