// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:${Versions.gradleVersion}")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlinVersion}")
        classpath("org.jetbrains.kotlin:kotlin-serialization:${Versions.kotlinVersion}")
        classpath("androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.safeArgsPlugin}")
        classpath("com.google.dagger:hilt-android-gradle-plugin:${Versions.hiltPlugin}")
    }
}

allprojects {
    repositories {
        google()
        mavenLocal()
        maven(url = ("https://jitpack.io"))
        jcenter()
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}